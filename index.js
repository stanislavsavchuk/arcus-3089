var map;
// var FeatureCollection = {
//     "type": "FeatureCollection",
//     "features": []
// };
var layers = [];
var coordinates = [];
var main_func = function () {};

// var greinerHormann = require('greiner-hormann');
var martinez = require('martinez-polygon-clipping');

var hull = require('hull');

var graham_scan_js = require('graham_scan_js');

var concaveman = require('concaveman');

var turf_merge = require('turf-merge');

var turf_aggregate = require('turf-aggregate');

var turf_convex = require('turf-convex');

var geojson_polygon_aggregate = require('geojson-polygon-aggregate');


var currentdate = new Date();
var datetime = "Last Build: " + currentdate.getDate() + "/"
    + (currentdate.getMonth() + 1) + "/"
    + currentdate.getFullYear() + " @ "
    + currentdate.getHours() + ":"
    + currentdate.getMinutes() + ":"
    + currentdate.getSeconds();
console.log(datetime);

function initMap(id) {
    map = L.map(id);
    L.tileLayer('https://api.mapbox.com/styles/v1/stassavchuk93/ciolhd2bi003n22nirqv95n09/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3Rhc3NhdmNodWs5MyIsImEiOiJjaW9saDdvd3QwMDF3dmxseTkyZzVlN295In0.espQjLLdlsjhvPIkNspGPw', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,

        id: 'ciolhd2bi003n22nirqv95n09',
        accessToken: 'pk.eyJ1Ijoic3Rhc3NhdmNodWs5MyIsImEiOiJjaW9saDdvd3QwMDF3dmxseTkyZzVlN295In0.espQjLLdlsjhvPIkNspGPw'
    }).addTo(map);

    map.setView([51.5, 0.1], 12);


    // leaflet draw
    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    // Initialise the draw control and pass it the FeatureGroup of editable layers
    var drawControl = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        }
    });
    map.addControl(drawControl);


    map.on('draw:created', function (e) {
        var type = e.layerType,
            layer = e.layer;

        if (type === 'polygon') {
            var polygon = L.polygon(layer._latlngs);

            polygon.addTo(map);

            layers.push(polygon);

            // coordinates.push(
            //   [layer._latlng]
            // );
            var c = [];
            for (var i = 0; i < layer._latlngs[0].length; i++) {
                c.push(
                    [
                        layer._latlngs[0][i].lat,
                        layer._latlngs[0][i].lng,
                    ]
                );
            }
            coordinates.push(c);

        }
        main_func();
    });

}


function sortFunction(a, b) {
    if (a[0] === b[0]) {
        if (a[1] === b[1]) {
            return 0;
        }
        else {
            return (a[1] < b[1]) ? -1 : 1;
        }
    }
    else {
        return (a[0] < b[0]) ? -1 : 1;
    }
}


function populeateLine(input_arr, n) {
    var arr = input_arr[0].slice();
    var res = [];

    for (var i = 0; i < arr.length - 1; i++) {
        res.push(arr[i]);
        var x0 = arr[i][0],
            y0 = arr[i][1],
            x1 = arr[i + 1][0],
            y1 = arr[i + 1][1];
        for (var j = 1; j <= n; j++) {

            var x = x0 + (x1 - x0) * (j / (n));
            var y = y0 + (y1 - y0) * (j / (n));

            res.push([x, y]);
        }
    }


    res.push(arr[arr.length - 1]);
    var x0 = arr[arr.length - 1][0],
        y0 = arr[arr.length - 1][1],
        x1 = arr[0][0],
        y1 = arr[0][1];
    for (var j = 1; j <= n; j++) {

        var x = x0 + (x1 - x0) * (j / (n));
        var y = y0 + (y1 - y0) * (j / (n));

        res.push([x, y]);
    }

    return [res];
}


initMap('map');

function intersection_destructive(a_, b_) {
    var a = a_.slice();
    var b = b_.slice();
    var result = [];
    while (a.length > 0 && b.length > 0) {
        if (a[0] < b[0]) {
            a.shift();
        }
        else if (a[0] > b[0]) {
            b.shift();
        }
        else /* they're equal */
        {
            result.push(a.shift());
            b.shift();
        }
    }

    return result;
}



function test_diff() {
    if (layers.length >= 2) {
        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var result = martinez.diff(p1, p2);

        map.removeLayer(layers[layers.length - 2]);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);
    }
}

function test_xor() {
    if (layers.length >= 2) {
        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var result = martinez.xor(p1, p2);

        map.removeLayer(layers[layers.length - 2]);
        map.removeLayer(layers[layers.length - 1]);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);
    }
}

function test_union() {
    if (layers.length >= 2) {
        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var result = martinez.union(p1, p2);

        map.removeLayer(layers[layers.length - 2]);
        map.removeLayer(layers[layers.length - 1]);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);
    }
}

function test_intersection() {
    if (layers.length >= 2) {
        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var result = martinez.intersection(p1, p2);

        map.removeLayer(layers[layers.length - 2]);
        map.removeLayer(layers[layers.length - 1]);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);
    }
}

function test_convex_hull_0() {
    if (layers.length >= 2) {

        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var pointSet = p1[0].concat(p2[0]);


        var convexHullSet = concaveman(pointSet, concavity = Infinity, lengthThreshold = 0);

        map.removeLayer(layers[layers.length - 1]);

        var result = martinez.diff([convexHullSet], p1);


        console.log('result', result);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);

    }
}

function test_concave_hull_0() {
    if (layers.length >= 2) {

        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var pointSet = p1[0].concat(p2[0]);


        var convexHullSet = concaveman(pointSet, concavity = 0, lengthThreshold = 0);

        map.removeLayer(layers[layers.length - 1]);

        var result = martinez.diff([convexHullSet], p1);


        console.log('result', result);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);

    }
}

function test_convex_hull_1() {
    if (layers.length >= 2) {

        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var pointSet = p1[0].concat(p2[0]);


        var convexHullSet = concaveman(pointSet, concavity = Infinity, lengthThreshold = 0);

        map.removeLayer(layers[layers.length - 1]);

        var result = martinez.diff([convexHullSet], p1);


        console.log('result', result);

        var res = [];
        if (result.length > 1) {

            for (var i = 0; i < result.length; i++) {
                if (
                    intersection_destructive(result[i], p2[0]).length > 0
                ) {
                    res.push(result[i]);
                    // console.log(result[i]);
                }
            }

            result = res;
        }

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);

    }
}

function test_concave_hull_1() {
    if (layers.length >= 2) {

        var p1 = layers[layers.length - 2].toGeoJSON().geometry.coordinates;
        var p2 = layers[layers.length - 1].toGeoJSON().geometry.coordinates;

        var pointSet = p1[0].concat(p2[0]);


        var convexHullSet = concaveman(pointSet, concavity = 0, lengthThreshold = 0);

        map.removeLayer(layers[layers.length - 1]);

        var result = martinez.diff([convexHullSet], p1);


        console.log('result', result);

        var res = [];
        if (result.length > 1) {

            for (var i = 0; i < result.length; i++) {
                if (
                    intersection_destructive(result[i], p2[0]).length > 0
                ) {
                    res.push(result[i]);
                    // console.log(result[i]);
                }
            }

            result = res;
        }

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);

    }
}


function test_aggregation() {
    if (layers.length >= 2) {

        console.log('Running "test9" function');
        var aggregate = geojson_polygon_aggregate;

        var p1 = layers[layers.length - 2].toGeoJSON();
        var p2 = layers[layers.length - 1].toGeoJSON();

         var featureCollection = {
         "type": "FeatureCollection",
         "features": [p1, p2]
         };

         var groups = $.extend({}, featureCollection);
         var data = $.extend({}, featureCollection);

        var result = aggregate.groups(groups, data, {
            'something': aggregate.reducers.sum('something'),
            'something-area-weighted': aggregate.reducers.areaWeightedSum('something'),
            'area': aggregate.reducers.totalArea(),
            'count': aggregate.reducers.count(),
            'arbitraryProperty': function (memo, feature) {
                // the aggregations above are provided for convenience, but you can
                // do whatever you want here. `memo` is the previous return value
                // of this function, or groups.properties['arbitraryProperty'] on the
                // first iteration.
                if (memo) {
                    return memo + ', ' + feature.properties['something']
                } else {
                    return 'Values: ' + feature.properties['something']
                }
            }
        });


        console.log('res', result);

        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Polygon",
                "coordinates": result
            }
        };

        var myStyle = {
            "color": "#ff7800",
            "weight": 5,
            "opacity": 0.65
        };

        var p = L.geoJson(geojsonFeature, {
            style: myStyle
        }).addTo(map);

    }
}

function changeF(func){
    $('#mapContainer').empty();
    $('#mapContainer').append('<div id="map" style="height: 500px"></div>');
    initMap('map');
    coordinates = [];
    layers = [];
    main_func = func;
}



$('#exampleRadios1').click(function () {
    changeF(test_diff);
});

$('#exampleRadios2').click(function () {
    changeF(test_xor);
});

$('#exampleRadios3').click(function () {
    changeF(test_union);
});

$('#exampleRadios4').click(function () {
    changeF(test_intersection);
});

$('#exampleRadios5').click(function () {
    changeF(test_convex_hull_0);
});

$('#exampleRadios6').click(function () {
    changeF(test_concave_hull_0);
});

$('#exampleRadios7').click(function () {
    changeF(test_convex_hull_1);
});

$('#exampleRadios8').click(function () {
    changeF(test_concave_hull_1);
});

$('#exampleRadios9').click(function () {
    changeF(test_aggregation);
});



